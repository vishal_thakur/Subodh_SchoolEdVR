﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControllerSetup : MonoBehaviour {

	string keyName = "none";
	System.Array values;
	[SerializeField] Text PressedTextRef; 
	[SerializeField] Text AllKeysTextRef; 

	bool DebugMode = false;

	[SerializeField]
	Button GazedAtButton;



	[SerializeField]
	Toggle GazedAtToggle;


	[SerializeField]
	TeleportationManager TeleportationManagerRef;

	void Start(){

		values = System.Enum.GetValues(typeof(KeyCode));

//		Messenger.AddListener <Button>(Events.OnObjectGazeEnter , OnObjectGazedEnter);
//		Messenger.AddListener (Events.OnObjectGazeExit , OnObjectGazedExit);
//		foreach (KeyCode key in values) {
//					Debug.LogError ("Keyfound");
//		AllKeysTextRef.text = "Step 1: Connect your controller to this mobile\n\n" +
//			"Step 2: Hit All keys one by one, You will see the respective name at the bottom of the screen\n\n" + 
//		"Step 3: Note these Keys and tell me so that I can understand the mapping!";


//						AllKeysTextRef.text += key.ToString () + "\n";
//		}
		
	}




	void ToggleTeleportationManager(bool flag){
		if (TeleportationManagerRef != null) {
			Globals.CanTeleport = flag;
			TeleportationManagerRef.enabled = flag;
		}
//		else
//			Debug.LogError ("Teleportation Manager is null here");
	}

	public void OnObjectGazedEnter(Button button){
		GazedAtButton = button;
		ToggleTeleportationManager (false);
	}

	public void OnObjectGazedEnter(Toggle toggle){
		ToggleTeleportationManager (false);
		GazedAtToggle = toggle;
	}

	public void OnObjectGazedExit(){
		GazedAtButton = null;
		GazedAtToggle = null;
//		try{
		if (this.gameObject.activeSelf) {
			this.StopAllCoroutines ();
			StartCoroutine (ReEnableTeleportation ());
		}
//		}
//		catch(System.Exception e){
//			Debug.LogError (e.Message.ToString());
//		}
	}


	void OnControllerClicked(){
		Messenger.Broadcast (Events.OnControllerKeyDown);

//		Debug.LogError (keyName);
		if (GazedAtButton != null) {
			GazedAtButton.onClick.Invoke ();
			GazedAtButton = null;
		}

		if (GazedAtToggle != null) {
			GazedAtToggle.isOn = !GazedAtToggle.isOn;
		}


		if (this.gameObject.activeSelf) {
			this.StopAllCoroutines ();
			StartCoroutine (ReEnableTeleportation ());
		}

	}

	public IEnumerator ReEnableTeleportation(){
		yield return new WaitForSeconds (2);

		ToggleTeleportationManager (true);
		this.StopAllCoroutines ();
	}

	void Update(){
//		}
//		return;

		if (Input.GetAxis ("Vertical") > 0)
			keyName = "Joystick UP";
		else if (Input.GetAxis ("Vertical") < 0)
			keyName = "Joystick Down";
		else if (Input.GetAxis ("Horizontal") > 0)
			keyName = "Joystick Right";
		else if (Input.GetAxis ("Horizontal") < 0)
			keyName = "Joystick Left";
		else

		foreach (KeyCode code in values) {		
			if (Input.GetKeyUp (code)) {
				OnControllerClicked ();
				keyName = System.Enum.GetName (typeof(KeyCode), code);
//				PressedTextRef.text = "Pressed key: " + keyName;
				break;
				Debug.LogError (keyName);
			
				}	
			}
			
	

//		if (DebugMode) {
//			//Set Key name
//			PressedTextRef.text = "You Pressed : " + keyName;
//		}
	}
		



//	void OnGUI(){
//		GUILayout.TextField ("Key" + keyName);
//	}
//

}

