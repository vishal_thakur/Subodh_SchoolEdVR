﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {

	[SerializeField] 
	TeleportationManager TeleportationManagerRef;


	System.Array values;


	void Start(){
		values = System.Enum.GetValues(typeof(KeyCode));

		Messenger.AddListener<Globals.GameModes> (Events.OnGameModeChanged , OnGameModeChanged);

		Messenger.AddListener <bool>(Events.OnPaused , OnGamePaused);
	}



	/// <summary>
	/// Raises the game paused event.
	/// </summary>
	void OnGamePaused(bool flag){
		//Paused
		if (flag)
			TeleportationManagerRef.enabled = false;
		//Not Paused
		else
			TeleportationManagerRef.enabled= true;
	}

	void OnGameModeChanged(Globals.GameModes gameMode){
		switch (gameMode) {
		case Globals.GameModes.Help:
//			ToggleTeleportationMode (false);
			break;
		case Globals.GameModes.Normal:
//			ToggleTeleportationMode (true);
			break;
		}
	}

	//Disable or Enable Teleportation
	public void ToggleTeleportationMode(bool flag){
		TeleportationManagerRef.enabled = flag;
	}


	string keyName = "Lawda";

	void Update(){
		if (Input.GetKeyUp (KeyCode.Space))
			ToggleTeleportationMode (false);
		
		if (Input.GetKeyUp (KeyCode.RightControl))
			ToggleTeleportationMode (true);


		foreach (KeyCode code in values) {	
			if (Input.GetKeyUp(code)) {	
				keyName = System.Enum.GetName (typeof(KeyCode), code);
//				Debug.LogError (keyName);
				switch (keyName) {
				//Start Button , Open Pause Menu
				case "Joystick1Button9":
					Messenger.Broadcast<bool> (Events.OnPaused , true);
					//					Debug.LogError ("Pause Menu");
					break;
					//Start Button , Open Pause Menu
				case "Escape":
					Messenger.Broadcast<bool> (Events.OnPaused , true);
					//					Debug.LogError ("Pause Menu");
					break;
				default:
//					Debug.LogError ("No Key Pressed");
					break;
				}
			}

		}

	}
}
