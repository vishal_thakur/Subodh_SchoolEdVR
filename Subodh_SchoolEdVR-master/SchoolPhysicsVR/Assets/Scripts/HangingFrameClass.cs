﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HangingFrameClass {


	public float x = 0;

	public float y = 0;


	public float T1 = 0;

	public float T2 = 0;

	public float mg = 0;

	public float L = 0;


}
