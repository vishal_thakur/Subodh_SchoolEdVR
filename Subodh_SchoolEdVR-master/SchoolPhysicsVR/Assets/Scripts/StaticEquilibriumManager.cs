﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using GoogleVR.HelloVR;

public class StaticEquilibriumManager : GenericMonoBehaviourSingleton<StaticEquilibriumManager>{
	[SerializeField] HangingFrameClass hangingFrameClass;


	[SerializeField] Text HangingFrameParamXTextRef;
	[SerializeField] Text HangingFrameParamYTextRef;
	[SerializeField] Text HangingFrameParamT1TextRef;
	[SerializeField] Text HangingFrameParamT2TextRef;
	[SerializeField] Text HangingFrameParammgTextRef;

	[SerializeField] Image TensionT1ArrowRef;
	[SerializeField] Image TensionT2ArrowRef;

	[Space(10) , SerializeField] ObjectController HookLeft;
	[SerializeField] ObjectController HookRight;


	[SerializeField] SkinnedMeshRenderer RopeLeft;
	[SerializeField] SkinnedMeshRenderer RopeRight;

	[SerializeField] float MaxAllowedDistanceBetweenHooks;
	[SerializeField] float MinAllowedDistanceBetweenHooks;


	[SerializeField] public SuspendedWeight SuspendedWeightRef;





	public bool IsInteractible {
		get{ return IsInteractible;}
		set{ 
			HookLeft.GetComponent<BoxCollider> ().enabled = value;
			HookRight.GetComponent<BoxCollider> ().enabled = value;
		}
	}


	public bool isDiagramViewActive;

	public bool CanGoAway = true;
	public bool CanComeClose = true;
	public bool UseMovementLimits = true;

	public AvailableHooks ActiveHook = AvailableHooks.None;
	public AvailableHooks CurrentlyGazedHook = AvailableHooks.None;


	LineRenderer LineRendererRef;


	[SerializeField] Transform MidPointObj;

	[System.Serializable]
	public enum AvailableHooks{
		Left,
		Right,
		None
	}

	public void ToggleDiagramView(bool flag){
		isDiagramViewActive = flag;
	}


	public void OnHookBeingGazed(int hookType){
		CurrentlyGazedHook = (AvailableHooks)hookType;
	}



//	public void OnGUI(){
////		if (Input.GetAxis ("Fire1") == 1)
////			GUILayout.TextField ("Fire1");
////
//		if (Input.GetAxis ("Fire2") == 1)
//			GUILayout.TextField ("Fire2");
//
//		if (Input.GetAxis ("Submit") == 1)
//			GUILayout.TextField ("Submit");
//	}
//
//	// Use this for initialization
	void Start () {
		OnHookSelected (AvailableHooks.None);
		LineRendererRef = GetComponent<LineRenderer> ();
		RelocateMidPointObj ();

	}

	public void OnHookSelected(AvailableHooks hook){
		ActiveHook = hook;
		switch (ActiveHook ) {
		case AvailableHooks.Left:
			HookLeft.GetComponent<ObjectController> ().CanMove = true;
			HookRight.GetComponent<ObjectController> ().CanMove = false;
			break;
		case AvailableHooks.Right:
			HookLeft.GetComponent<ObjectController> ().CanMove = false;
			HookRight.GetComponent<ObjectController> ().CanMove = true;
			break;
		case AvailableHooks.None:
			HookLeft.GetComponent<ObjectController> ().CanMove = false;
			HookRight.GetComponent<ObjectController> ().CanMove = false;
			break;
		}
	}

//	IEnumerator RestartScene(){
//		yield return new WaitForSeconds (5);
//		SceneManager.LoadScene("ClassroomVR");
//	}
	
	// Update is called once per frame
	void Update () {
		FixAngles ();
		//Restart
//		if (Input.GetButtonUp("Submit"))
//			SceneManager.LoadScene("ClassroomVR");

//		if (Input.GetAxis("Fire1") == 1) {
//			int randNo = Random.Range (0, 5);
//			if(randNo> 2){
//				if (ActiveHook == StaticEquilibriumManager.AvailableHooks.Left)
//					HookLeft.gameObject.SetActive (false);
//			}
//			else{
//				if (ActiveHook== StaticEquilibriumManager.AvailableHooks.Right)
//					HookRight.gameObject.SetActive (false);
//			}
//		}

//		if (Input.GetAxis("Fire2") == 1 || Input.GetAxis("Submit") == 1) {
//			if(CurrentlyGazedHook == AvailableHooks.Left){
//				HookLeft.GetComponent<ObjectController> ().OnSelected ();
//			}
//			else if(CurrentlyGazedHook == AvailableHooks.Right){
//				HookRight.GetComponent<ObjectController> ().OnSelected ();
//			}
//		}

//		Debug.LogError (Vector3.Distance(HookLeft.position , HookRight.position).ToString());

//		RelocateMidPointObj ();

		//
//		HookLeft.GetComponent<LineRenderer> ().SetPosition (2 , HookRight.transform.position);



		//Midpoint To Left Extreme Which is left half line
		MidPointObj.GetComponent<LineRenderer>().SetPosition (0 , MidPointObj.transform.position);
		MidPointObj.GetComponent<LineRenderer>().SetPosition (1 , (SuspendedWeightRef.LeftExtremePoint.transform.position + SuspendedWeightRef.RightExtremePoint.transform.position)/2);



		//Line : Left Hook To Mid Point 
		RopeLeft.GetComponent<LineRenderer>().SetPosition (0 , HookLeft.transform.position);
		RopeLeft.GetComponent<LineRenderer>().SetPosition (1 , MidPointObj.position);

		//Line : Right Hook To Mid Point 
		RopeRight.GetComponent<LineRenderer>().SetPosition (0 , HookRight.transform.position);
		RopeRight.GetComponent<LineRenderer>().SetPosition (1 , MidPointObj.position);

		//Horizontal Line : Through The Hanging Rod
		LineRendererRef.SetPosition (0 , SuspendedWeightRef.LeftExtremePoint.transform.position);
		LineRendererRef.SetPosition (1 , SuspendedWeightRef.RightExtremePoint.transform.position);

		RopeLeft.enabled = !isDiagramViewActive;
		RopeRight.enabled = !isDiagramViewActive;

		LineRendererRef.enabled = isDiagramViewActive;


		RopeLeft.GetComponent<LineRenderer> ().enabled = isDiagramViewActive;
		RopeRight.GetComponent<LineRenderer> ().enabled = isDiagramViewActive;
		MidPointObj.GetComponent<LineRenderer> ().enabled = isDiagramViewActive;


		if (!UseMovementLimits)
			return;

		float distanceBetweenHooks = Vector3.Distance (HookLeft.transform.position, HookRight.transform.position); 

		//If Distance Is Greater Than Max then Limit Depart movement
		if (distanceBetweenHooks >= MaxAllowedDistanceBetweenHooks) {
			CanGoAway = false;
			CanComeClose = true;
		}
		else {
			CanGoAway = true;
//			CanComeClose = true;
		}

//		Debug.LogError (Vector3.Distance (HookLeft.transform.position, HookRight.transform.position).ToString());

		//If Distance Is Greater Than Max then Limit Depart movement
		if (distanceBetweenHooks <= MinAllowedDistanceBetweenHooks) {
			CanGoAway = true;
			CanComeClose = false;
		} 
		else {
//			CanGoAway = true;
			CanComeClose = true;
		
		}
//
//		//If Distance Is Greater Than Max then Limit Depart movement
//		if (Vector3.Distance (HookLeft.transform.position, HookRight.transform.position) < MinAllowedDistanceBetweenHooks) {
//			CanGoAway = true;
//			CanComeClose = false;
//		}
//
////		//If Distance Is less Than min then Limit Closeure movement
//		else if(Vector3.Distance(HookLeft.position , HookRight.position) >= MinAllowedDistanceBetweenHooks){
//			CanGoAway = true;
//			CanComeClose = false;
//		}
		CalculateHangingFrameParams();
	}

//
	void RelocateMidPointObj(){
		MidPointObj.transform.position = (HookLeft.transform.position + HookRight.transform.position)/2;
	}


	void CalculateHangingFrameParams(){
		//Calculate x
		hangingFrameClass.x = (float)System.Math.Round((double)Vector3.Distance(HookLeft.transform.position , MidPointObj.position) , 2);
		HangingFrameParamXTextRef.text = "X = " + hangingFrameClass.x;

		//Calculate y
		hangingFrameClass.y =   (float)System.Math.Round((double)Vector3.Distance(HookRight.transform.position , MidPointObj.position), 2);
		HangingFrameParamYTextRef.text = "Y = " + hangingFrameClass.y;

		//Calculate T1
		hangingFrameClass.T1 = (float)System.Math.Round((double)((hangingFrameClass.mg * hangingFrameClass.y) / (hangingFrameClass.x + hangingFrameClass.y)) , 2);
		HangingFrameParamT1TextRef.text = "T1 = " + hangingFrameClass.T1;

		//Calculate T2
		hangingFrameClass.T2 = (float)System.Math.Round((double)((hangingFrameClass.mg * hangingFrameClass.x) / (hangingFrameClass.x + hangingFrameClass.y)),2);
		HangingFrameParamT2TextRef.text = "T2 = " + hangingFrameClass.T2;


//
//		//Set Rope Tension Arrows Visuals
//
//		if (ActiveHook == AvailableHooks.Left) {
//			if (Input.GetAxis ("Horizontal") > 0)
//				TensionT1ArrowRef.fillAmount = Mathf.Lerp (TensionT1ArrowRef.fillAmount, 1, Time.deltaTime * 0.25f);
//			else if (Input.GetAxis ("Horizontal") < 0)
//				TensionT1ArrowRef.fillAmount = Mathf.Lerp (TensionT1ArrowRef.fillAmount, 0.409f, Time.deltaTime * 0.25f);
//		}
//		else if (ActiveHook == AvailableHooks.Right) {
//			if (Input.GetAxis ("Horizontal") < 0)
//				TensionT2ArrowRef.fillAmount = Mathf.Lerp (TensionT2ArrowRef.fillAmount, 1, Time.deltaTime * 0.25f);
//			else if (Input.GetAxis ("Horizontal") > 0)
//				TensionT2ArrowRef.fillAmount = Mathf.Lerp (TensionT2ArrowRef.fillAmount, 0.409f, Time.deltaTime * 0.25f);
//		}
		//Set Rope Tension Arrows Visuals

//		if (ActiveHook == AvailableHooks.Left) {
		if (Input.GetAxis ("Horizontal") > 0) {
			TensionT1ArrowRef.fillAmount = Mathf.Lerp (TensionT1ArrowRef.fillAmount, 1, Time.deltaTime * 0.2f);
			TensionT2ArrowRef.fillAmount = Mathf.Lerp (TensionT2ArrowRef.fillAmount, 0.409f, Time.deltaTime * 0.2f);
		}
		else if (Input.GetAxis ("Horizontal") < 0) {
			TensionT1ArrowRef.fillAmount = Mathf.Lerp (TensionT1ArrowRef.fillAmount, 0.409f, Time.deltaTime * 0.2f);
			TensionT2ArrowRef.fillAmount = Mathf.Lerp (TensionT2ArrowRef.fillAmount, 1, Time.deltaTime * 0.2f);
		}
//		}
//		else if (ActiveHook == AvailableHooks.Right) {
//			if (Input.GetAxis ("Horizontal") < 0)
//				TensionT2ArrowRef.fillAmount = Mathf.Lerp (TensionT2ArrowRef.fillAmount, 1, Time.deltaTime * 0.25f);
//			else if (Input.GetAxis ("Horizontal") > 0)
//				TensionT2ArrowRef.fillAmount = Mathf.Lerp (TensionT2ArrowRef.fillAmount, 0.409f, Time.deltaTime * 0.25f);
//		}
//		TensionT2ArrowRef.fillAmount = Mathf.Clamp((TensionT2ArrowRef.fillAmount / 2) * (hangingFrameClass.T2/20) , .409f , 1 );
//
//		//Calculate T1
//		hangingFrameClass.T1 = (float)System.Math.Round((double)((hangingFrameClass.mg/2)));
//		HangingFrameParamT1TextRef.text = "T1 = " + hangingFrameClass.T1;
//		//Calculate T1
//		hangingFrameClass.T2 = (float)System.Math.Round((double)((hangingFrameClass.mg/2)));
//		HangingFrameParamT2TextRef.text = "T2 = " + hangingFrameClass.T2;
//
		//mg and L
		HangingFrameParammgTextRef.text = "mg = " + hangingFrameClass.mg;

		if (Globals.CurrentState == Globals.TorqueSceneGameState.InteractAndChangeXandYOfHangingFrame) {
			Globals.LiveText = "You can vary x and y from 0 to L/2 on both sides and see the effect on T1 and T2.\n\n" + "T1 = " + "mg.y/x+y = " + hangingFrameClass.T1 + "\n\n" +
			"T2 = " + "mg.x/x+y = " + hangingFrameClass.T2;
		}
		else if (Globals.CurrentState == Globals.TorqueSceneGameState.ExplainRopeLengthVaryEffectsOnSystem) {
			Globals.LiveText = "If length of the ropes vary then the rod makes some angle θ with the horizontal line.\n" +
				
				"1)  ΣF = 0\n" + 
				"T1 + T2 - mg = 0            -> 1\n" + 

				"1)  ΣTO = 0\n" + 
				"TorqueT1 + TorqueT2 - Torquemg = 0        \n" + 

				"T1 x XCosθ( " + (int)HookLeft.BottomHookJointRef.GetComponent<AngleCalculator>().angle + ") - T2 x YCosθ(" +  (int)HookRight.BottomHookJointRef.GetComponent<AngleCalculator>().angle + " + 0 = 0 \n" +

				"T1X = T2Y                            ->2\n\n" +

				"Solving equations 1 and 2, we get\n" + 

				"T1 = " + "mg.y/x+y = " + hangingFrameClass.T1 +

				"\nT2 = " + "mg.x/x+y = " + hangingFrameClass.T2;
		}
	}


	void FixAngles(){
		AngleCalculator leftAngleObj = HookLeft.BottomHookJointRef.GetComponent<AngleCalculator> ();
		AngleCalculator RightAngleObj = HookRight.BottomHookJointRef.GetComponent<AngleCalculator> ();

		RightAngleObj.Fixedangle = (int)RightAngleObj.angle;
		leftAngleObj.Fixedangle = 180 - (int)RightAngleObj.angle;
//		Debug.LogError ("Left Angle" + leftAngleObj.Fixedangle + "\t\t" + "Right angle:\t" + (int)RightAngleObj.angle);

	}

}
