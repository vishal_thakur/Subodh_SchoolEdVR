﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SciFiEquationManager : MonoBehaviour {


	[SerializeField] GameObject SciFiVisualsRef;
	// Use this for initialization
	void Start () {
		ToggleVisuals (false);

		Messenger.AddListener<Globals.GameModes> (Events.OnGameModeChanged , OnGameModeChanged);
	}

	void OnGameModeChanged(Globals.GameModes gameMode){
		switch (gameMode) {
		case Globals.GameModes.Help:
			ToggleVisuals (false);
			break;
		case Globals.GameModes.Normal:
			ToggleVisuals (true);
			break;
		}
	}



	public void ToggleVisuals(bool flag){
		SciFiVisualsRef.SetActive (flag);
	}

}
