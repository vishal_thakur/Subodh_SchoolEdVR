﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HintObject : MonoBehaviour {
	
	public string Name;

	//Corresponding Vectors
	public Vector3 DesiredInfoPopupPosition;
	public Quaternion DesiredInfoPopupRotation;
	public Vector3 DesiredAstroBoyPosition;

	//Material Refs
	public Material inactiveMaterial;
	public Material gazedAtMaterial;

	//Hint Image and Text as Specified by the User
	public Sprite HintInfoImage;
	public string HintInfoText;


	//Extra GameObjects That needs to be activated/Deactivated
	public GameObject[] ExtraEnableGameObjects;



	//Private Params
	bool isGazedAt = false;
	Renderer MatRenderer;
	bool ShowingCLickedEffect = false;

	Material ClickedMode;
	Material HoveredMode;
	Material NormalMode;





	// Use this for initialization
	void Start () {
		MatRenderer = GetComponent<Renderer>();

		NormalMode = Resources.Load ("NormalInfo")as Material;
		ClickedMode = Resources.Load ("ClickedInfo")as Material;
		HoveredMode = Resources.Load ("HoverInfo")as Material;
	}



	public void SetGazedAt(bool gazedAt) {
		if (ShowingCLickedEffect)
			return;

		isGazedAt = gazedAt;
		if (inactiveMaterial != null && gazedAtMaterial != null) {
			MatRenderer.material = gazedAt ? HoveredMode : NormalMode;
			return;
		}
	}

	public void OnClicked(){
		StartCoroutine(ShowClickedEffectForAWhile());

	}



	IEnumerator ShowClickedEffectForAWhile(){
		ShowingCLickedEffect = true;

		MatRenderer.material = ClickedMode;

		yield return new WaitForSeconds(.3f);

		MatRenderer.material = HoveredMode;

		yield return new WaitForSeconds(.3f);

		MatRenderer.material = ClickedMode;

		yield return new WaitForSeconds(.3f);

		MatRenderer.material = NormalMode;

		ShowingCLickedEffect = false;


		Messenger.Broadcast<HintObject>(Events.OnHintObjectClicked , this);
		StopCoroutine (ShowClickedEffectForAWhile());
	}
}
