﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempAnimator : MonoBehaviour {

	[SerializeField] Animator AnimatorRef;


	void Start(){
		Messenger.AddListener<HintObject>(Events.OnHintObjectClicked , OnHintObjectClicked);
	}


	void OnHintObjectClicked(HintObject obj){
		AnimatorRef.SetTrigger ("Anticlockwise");
	}
}
