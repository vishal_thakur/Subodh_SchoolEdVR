﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HintSystemManager : MonoBehaviour {

	//Info popup Ref
	[SerializeField] GameObject InfoPopupUI;

	[SerializeField] Image InfoPopupImageRef;

	[SerializeField] Text InfoPopupTextRef;


	[SerializeField] GameObject HintSystemRef;


	[SerializeField] List<HintObject> AllHintObjects = new List<HintObject>();

	[SerializeField] HintObject LastSelectedHintObject;





	// Use this for initialization
	void Start () {
		//Disable Infopopup Initially
		InfoPopupUI.gameObject.SetActive (false);

		//Grab all Hint Objects
		AllHintObjects.Clear ();
		foreach (HintObject obj in GetComponentsInChildren<HintObject> ())
			AllHintObjects.Add (obj);
	
		//Listen To Hint object Selected Event
		Messenger.AddListener<HintObject> (Events.OnHintObjectClicked , OnHintObjectClicked);	

		Messenger.AddListener<Globals.GameModes> (Events.OnGameModeChanged , OnGameModeChanged);
	}




	void OnGameModeChanged(Globals.GameModes gameMode){
		switch (gameMode) {
		case Globals.GameModes.Help:
//			Debug.LogError ("Help mode");
			HintSystemRef.SetActive (true);
			break;
		case Globals.GameModes.Normal:
//			Debug.LogError ("Normal mdoe");
			HintSystemRef.SetActive (false);
			break;
		}
	}





	//Handle Hint Object selected Callback
	void OnHintObjectClicked(HintObject hintObject){
		//Hide Current Hint Object annd show it when new one is selected
		if(LastSelectedHintObject != null)
			LastSelectedHintObject.gameObject.SetActive (true);




		//Disble Hint Object So that Only the infopoup is visible
		hintObject.gameObject.SetActive (false);
		if (LastSelectedHintObject != null) {
			foreach (GameObject obj in LastSelectedHintObject.ExtraEnableGameObjects)
				obj.SetActive (false);
		}

		//Save it as the last selected Hint Object
		LastSelectedHintObject = hintObject;
		if (LastSelectedHintObject != null) {
			foreach (GameObject obj in hintObject.ExtraEnableGameObjects)
				obj.SetActive (true);
		}


		//Enable Info popup
		InfoPopupUI.SetActive (true);

		//Reposition info popup to new position
		InfoPopupUI.transform.position = hintObject.DesiredInfoPopupPosition;
		InfoPopupUI.transform.rotation= hintObject.DesiredInfoPopupRotation;

		//Set new Infopopup Hint Image and text as per the hint object
		InfoPopupImageRef.sprite = hintObject.HintInfoImage;
		InfoPopupTextRef.text = hintObject.HintInfoText;
	}
}
