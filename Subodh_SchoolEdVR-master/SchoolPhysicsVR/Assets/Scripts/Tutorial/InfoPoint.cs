﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoPoint : MonoBehaviour {

	[SerializeField] GameObject InfoPopupGO;

	// Use this for initialization
	void Start () {
		InfoPopupGO.SetActive (false);
	}


	public void ShowInfo(){
		StartCoroutine (ShowInfoPopupForAWhile());
	}



	/// <summary>
	/// Shows the info popup for A while.
	/// </summary>
	/// <returns>The info popup for A while.</returns>
	IEnumerator ShowInfoPopupForAWhile(){
		InfoPopupGO.SetActive (true);

		yield return new WaitForSeconds (Globals.InfoPointTimeout);

		InfoPopupGO.SetActive (false);

		StopCoroutine (ShowInfoPopupForAWhile());
	}
}
