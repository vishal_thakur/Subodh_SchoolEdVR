﻿using UnityEngine;
using System.Collections;

public class CameraFacingBillboard : MonoBehaviour
{
	public Camera m_Camera;

	[SerializeField] float damping = 0.5f;
	public bool xAxis = true;
	public bool yAxis = true;
	public bool zAxis = true;
	public bool UpdateOnlyInitially = false;

	[SerializeField]bool XYZLookAT = true;

	void Start(){
		if(UpdateOnlyInitially)
			PerformBillboard ();
	}

	void Update()
	{
		if (UpdateOnlyInitially)
			return;

		PerformBillboard ();
	}


	void PerformBillboard(){
		if (XYZLookAT) {
			//			if (xAxis) {
			transform.LookAt (transform.position + m_Camera.transform.rotation * Vector3.forward,
				m_Camera.transform.rotation * Vector3.up);
			//			}
			//			else if (yAxis) {
			//				transform.LookAt (transform.position + m_Camera.transform.rotation * Vector3.forward,
			//					m_Camera.transform.rotation * Vector3.up);
			//		}
			//		else if (zAxis) {
			//				transform.LookAt (transform.position + m_Camera.transform.rotation * Vector3.forward,
			//					m_Camera.transform.rotation * Vector3.up);
			//			}
			return;
		}

		var lookPos = m_Camera.transform.position - transform.position;

		if(xAxis)
			lookPos.x = 0;

		if(yAxis)
			lookPos.y = 0;

		if(zAxis)
			lookPos.z = 0;


		var rotation = Quaternion.LookRotation(lookPos);
		transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * damping);
	}
}