﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenuManager : MonoBehaviour {


	[SerializeField] Transform SpawnPointInPlayerFOV;

	[SerializeField] 
	Button HelpAndNormalModeButton;

	[SerializeField]
	GameObject PauseMenu;

	[SerializeField]
	GameObject GameVisuals;




	void Start(){
		Messenger.AddListener<bool> (Events.OnPaused, TogglePauseMenu);

		//Disable Pause menu By default
		TogglePauseMenu (false);
	}





	public void TogglePauseMenu(bool flag){
		//Show/Hide Pause Menu 
		PauseMenu.SetActive (flag);

		GameVisuals.SetActive (!flag);


		//If SHow Pause Menu then reposition it in front of the Player FOV
		if (flag) {
			PauseMenu.transform.position = SpawnPointInPlayerFOV.position;
		}

		//Toggle HelpAndNormalModeButton as per the current Game Mode
//		ToggleHelpAndNormalButton (Globals.CurrentGameMode);
	}



	//Change Pause Menu Text For the Game Mode representer Button
	public void ToggleHelpAndNormalButton(){

//		//Change The current Game Mode
//		Globals.CurrentGameMode = gameMode;

		//Change Text
		switch (Globals.CurrentGameMode) {

		case Globals.GameModes.Help:
			HelpAndNormalModeButton.GetComponentInChildren<Text>().text = "Normal Mode";
			break;

		case Globals.GameModes.Normal:
			HelpAndNormalModeButton.GetComponentInChildren<Text>().text = "Help Mode";
			break;

		default:
			Debug.LogError ("Something wrong here!");
			break;
		}

		//BroadCast Game Mode Changed 
		Messenger.Broadcast<Globals.GameModes> (Events.OnGameModeChanged , Globals.CurrentGameMode); 

	}



	public void OnButtonPressed(string text){
		switch (text.ToLower()) {
		case "mainmenu":
			SceneManager.LoadScene ("MainMenu");
			break;
		case "exit":
			TogglePauseMenu (false);
			break;
		case "gamemode":
			int currentGameMode = (int)Globals.CurrentGameMode;
			//Go to next State
			currentGameMode++;

			//Overflow Prevention
			if (currentGameMode > 1)
				currentGameMode = 0;


			Globals.CurrentGameMode = (Globals.GameModes)currentGameMode;

			//Unpause Game
			Messenger.Broadcast<bool> (Events.OnPaused , false);

			ToggleHelpAndNormalButton ();

			break;
		}
	
	}
		
//	void OnGUI(){
//		GUILayout.TextField (Globals.CurrentGameMode.ToString());
//	}


	/// <summary>
	/// Raises the game resume event.
	/// </summary>
	public void OnGameResume(){
		Messenger.Broadcast<bool> (Events.OnPaused, false);
	}

}
