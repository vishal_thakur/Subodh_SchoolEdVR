﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MidPlacement : MonoBehaviour {

	[SerializeField] Transform Point1;
	[SerializeField] Transform Point2;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = (Point1.position + Point2.position) / 2;
	}
}
