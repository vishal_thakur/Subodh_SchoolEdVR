﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GoogleVR.HelloVR;

public class TorqueManager : MonoBehaviour {

	bool isDoorOpen = false;

	[SerializeField] LineRenderer LineRendererRef; 

	[SerializeField] GameObject DoorTorqueVisuals;
	[SerializeField] GameObject ForceTorqueVisuals;



	[SerializeField] GameObject ClockwiseArrows;
	[SerializeField] GameObject AntiClockwiseArrows;

	[SerializeField] GameObject DoorOpenForceVisuals;
	[SerializeField] GameObject DoorCloseForceVisuals;

	[SerializeField] Animator DoorAnimator;
	[SerializeField] Animator TorqueArrows;
	[SerializeField] Animator AstroBoyOpenDoor;
	[SerializeField] Animator AstroBoyCloseDoor;

	[SerializeField] Transform PivotPoint;
	[SerializeField] Transform ForcePoint;


	[SerializeField] GameObject OpenDoorInteractionUI;
	[SerializeField] GameObject CloseDoorInteractionUI;

	[SerializeField] Button OpenDoorButton;
	[SerializeField] Button CloseDoorButton;


	/// <summary>
	/// Door state.
	/// </summary>
	public enum DoorState{
		Open,
		Close
	}

	//------------------------------------------------------------------------------------------------------------



	void Update(){
		//Draw Line B/W Pivot And Force Point
		LineRendererRef.SetPosition (0 , PivotPoint.position);
		LineRendererRef.SetPosition (1 , ForcePoint.position);
	}

//	void OnGUI(){
//		if (GUILayout.Button ("test")) {
////			OpenDoorButton.onClick.remo ();
////			CloseDoorButton.onClick.RemoveAllListeners ();
//		}
//	}
//

	/// <summary>
	/// Awake this instance.
	/// </summary>
	void Start(){
		SceneFlowManager SceneFlowManagerRef = GameObject.FindObjectOfType<SceneFlowManager>(); 

		OpenDoorButton.onClick.AddListener(SceneFlowManagerRef.NextState);
		CloseDoorButton.onClick.AddListener(SceneFlowManagerRef.NextState);
		//Disable Torque and Force Visuals Initially as these forces are intentional not natural in our case
		DoorTorqueVisuals.SetActive (false);
		ForceTorqueVisuals.SetActive (false);

		OpenDoorInteractionUI.SetActive (false);
		CloseDoorInteractionUI.SetActive (false);


//		ForcePoint.GetComponent<LineRenderer> ().SetPosition (0 , PivotPoint.position);
//		ForcePoint.GetComponent<LineRenderer> ().SetPosition (1 , ForcePoint.position);
	}


	public void RemoveAllListenersFromButton(Button button){
		button.onClick.RemoveAllListeners ();
	}


	public void Initialize(){
		OpenDoorInteractionUI.SetActive (true);
	}




//	void OnGUI(){
//		if(GUILayout.Button("Open"))
//			OpenDoor();
//		if(GUILayout.Button("Close"))
//			CloseDoor();
//	}

	/// <summary>
	/// Opens the door.
	/// </summary>
	public void OpenDoor(){
		//Donot Open Door if its open already!
		if (isDoorOpen)
			return;

		StopCoroutine ("DoorBehaviour");
		//Animate Kid to open Door 
		AstroBoyOpenDoor.SetTrigger("OpenDoor");

		//Show Torque Visuals In AntiClockwise Direction
		StartCoroutine(DoorBehaviour(DoorState.Open));
	}

	/// <summary>
	/// Closes the door.
	/// </summary>
	public void CloseDoor(){
		//Donot Close Door if its closed already!
		if (!isDoorOpen)
			return;

		StopCoroutine ("DoorBehaviour");
		//Animate Kid to Close Door
		AstroBoyCloseDoor.SetTrigger("OpenDoor");

		//Show Torque Visuals In Clockwise Direction
		StartCoroutine(DoorBehaviour(DoorState.Close));

	}


	/// <summary>
	/// Doors the behaviour.
	/// </summary>
	/// <returns>The behaviour.</returns>
	/// <param name="state">State.</param>
	IEnumerator DoorBehaviour(DoorState state){
//		DoorAnimator.SetTrigger("Reset");
		//Show Door Torque  and Force Visuals
		DoorTorqueVisuals.SetActive (true);
		ForceTorqueVisuals.SetActive (true);

		//Set Force Point To Under Force Color
		ForcePoint.GetComponent<InteractibleObj> ().SetGazedAt (true);

		//Do Desired Behaviour as per Door State
		switch (state) {
		case DoorState.Open:

			OpenDoorInteractionUI.SetActive (false);
			DoorAnimator.SetTrigger ("Open");
			TorqueArrows.SetTrigger ("Anticlockwise");

			AntiClockwiseArrows.gameObject.SetActive (true);
			ClockwiseArrows.gameObject.SetActive (false);

			DoorOpenForceVisuals.gameObject.SetActive (true);
			DoorCloseForceVisuals.gameObject.SetActive (false);
			isDoorOpen = true;
			break;
		case DoorState.Close:
			CloseDoorInteractionUI.SetActive (false);

			DoorAnimator.SetTrigger ("Close");
			TorqueArrows.SetTrigger ("Clockwise");

			AntiClockwiseArrows.gameObject.SetActive (false);
			ClockwiseArrows.gameObject.SetActive (true);

			DoorOpenForceVisuals.gameObject.SetActive (false);
			DoorCloseForceVisuals.gameObject.SetActive (true);

			isDoorOpen = false;
			break;
		}

		//Wait fot x seconds
		yield return new WaitForSeconds (8f);

		//Hide Door Force and Torque visuals as its in quilibrium now
		DoorTorqueVisuals.SetActive(false);
		ForceTorqueVisuals.SetActive(false);


		//Reset Force point To normal Color
		ForcePoint.GetComponent<InteractibleObj> ().SetGazedAt (false);

		switch (state) {
		case DoorState.Close:
			OpenDoorInteractionUI.SetActive (true);
			CloseDoorInteractionUI.SetActive (false);
			break;
		case DoorState.Open:
			OpenDoorInteractionUI.SetActive (false);
			CloseDoorInteractionUI.SetActive (true);
			break;
		}
	}




}
