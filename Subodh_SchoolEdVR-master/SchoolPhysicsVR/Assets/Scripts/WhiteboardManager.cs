﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Crosstales.RTVoice.Tool;
using Crosstales.RTVoice;
using Crosstales.RTVoice.Model.Event;

public class WhiteboardManager : MonoBehaviour {
	//All topics to be narrated
	[SerializeField] List<WhiteboardTopic> Topic = new List<WhiteboardTopic>();

	//Used to feed string data which is to be narrated verbally
//	[SerializeField] SpeechText SpeechTextRef;

	[SerializeField] test TTSSpeakerRef;

	[SerializeField] Animator AstroBoyRef;


	[SerializeField] Text WhiteBoardTitleTextRef;
	[SerializeField] Text WhiteBoardTextRef;

	[SerializeField] GameObject WhiteBoardCanvasRef;

	public bool ShowLiveText = false;

	[SerializeField]public int CurrentTopicIndex = 0;

	[SerializeField] int GameStateInitial;




	[SerializeField] WhiteboardTopic currentTopic;

	[SerializeField] GameObject NextButtonRef;

	[SerializeField] AudioSource cameraAudioSource;

	[SerializeField] GameObject TempSciFiPanel;

//	[SerializeField] float EarlyNotifierTimeCutterSeonds;



	void Awake(){
//		Speaker.OnSpeakStart+= OnTopicNarrationStart;


		//Save initial game State
		GameStateInitial = CurrentTopicIndex;
//		Globals.CurrentState = (Globals.TorqueSceneGameState)GameStateInitial;

		#if UNITY_EDITOR
		cameraAudioSource.mute = false;
		#else
		cameraAudioSource.mute = true;
		#endif
//		Speaker.OnSpeakComplete += OnTopicNarrationComplete;
	}

	public void ShowNextButton(){
		NextButtonRef.SetActive (true);
	}

	public void HideNextButton(){
		NextButtonRef.SetActive (false);
	}

	public void ShowCanvas(){
		WhiteBoardCanvasRef.SetActive (true);
	}

	public void HideCanvas(){
		WhiteBoardCanvasRef.SetActive (false);
	}

	public void ToggleTypeWritereffect(bool flag){
		WhiteBoardTextRef.GetComponent<TW_RandomPointer> ().enabled = flag;
	}

	void OnGUI(){
//		if (GUILayout.Button ("Speak"))
//			NarrateTopic (Topic[0]);
	}


//	public void RepeatTopic(){
//		NarrateTopic (Topic[CurrentTopicIndex]);
//	}

	public void NextTopic(){


		//Capping Overflow
		if (CurrentTopicIndex >= Topic.Count)
			CurrentTopicIndex = 0;
		
		NarrateTopic (Topic [CurrentTopicIndex]);

	}


	void Update(){
//		if (Input.GetKeyUp(KeyCode.Space))
//			NextTopic ();

		if (ShowLiveText) {
//			WhiteBoardTextRef.GetComponent<TW_RandomPointer>().enabled = false;
			WhiteBoardTextRef.text = Globals.LiveText;
		}	
//		else{			
//			WhiteBoardTextRef.GetComponent<TW_RandomPointer>().enabled = true;
//
//		}
	}



//	public void PreviousTopic(){
//		currentTopic = Topic [CurrentTopicIndex];
//		NarrateTopic (currentTopic);
//
//		//Increment To next Topic
//		CurrentTopicIndex--;
//
//		//Capping Overflow
//		if (CurrentTopicIndex < 0)
//			CurrentTopicIndex = Topic.Count - 1;
//	}


	void NarrateTopic(WhiteboardTopic topic){

		//**********Temp Way to schow The sci fi panel*************
//		if (!TempSciFiPanel.gameObject.activeSelf)
		TempSciFiPanel.GetComponent<SciFiEquationManager>().ToggleVisuals(true);
		
		//Set Current Topic
		currentTopic = topic;

		//Show Topic On Whiteboard as text

		if (!ShowLiveText) {
			WhiteBoardTitleTextRef.GetComponent<TW_RandomPointer> ().ORIGINAL_TEXT = WhiteBoardTitleTextRef.text = topic.Title;
			WhiteBoardTextRef.GetComponent<TW_RandomPointer> ().ORIGINAL_TEXT = WhiteBoardTextRef.text = topic.textForWhiteBoard;

			WhiteBoardTitleTextRef.GetComponent<TW_RandomPointer> ().StartTypewriter ();
			WhiteBoardTextRef.GetComponent<TW_RandomPointer> ().StartTypewriter ();
		}


		//Speak the whole topic

		//-----------TTS Method------
		//Feed Text
//		SpeechTextRef.Text = topic.TextForNarrator;
		//Speak Text
//		SpeechTextRef.Speak ();
		#if !UNITY_EDITOR
		TTSSpeakerRef.Speak(topic.TextForNarrator);
		#endif

		//-----------PreDef Audio method------------
		//Feed Audio
		cameraAudioSource.clip = topic.audio;
//		//Play Audio
		if (cameraAudioSource.isPlaying) {
			cameraAudioSource.Stop ();
			this.StopCoroutine("NotifyOnComplete");
		}

		#if !UNITY_EDITOR
		StartCoroutine(NotifyOnComplete((int)(topic.audio.length - 3)));
		#else
		StartCoroutine(NotifyOnComplete((int)(topic.audio.length)));
		#endif
		cameraAudioSource.Play();
		OnTopicNarrationStart ();
	}



	IEnumerator NotifyOnComplete(int seconds){
//		Debug.LogError ("Waiting for Seconds:" + seconds.ToString ());
//		yield return new WaitForSecondsRealtime(seconds * Globals.TimeScale);

		while (seconds > 0) {
			yield return new WaitForSecondsRealtime (1);
			seconds--;
//			Debug.LogError (seconds.ToString());
		}

//		Debug.LogError("Wait Complete");
		OnTopicNarrationComplete ();

		TempSciFiPanel.transform.rotation = new Quaternion (0 , 83.14f , 0 , 0);
		TempSciFiPanel.GetComponent<SciFiEquationManager>().ToggleVisuals(false);
//		TempSciFiPanel.gameObject.SetActive (false);
		this.StopCoroutine("NotifyOnComplete");
	}

	public void NarrateTopic(int topic){
		//**********Temp Way to schow The sci fi panel*************
//		if (!TempSciFiPanel.gameObject.activeSelf)
//		TempSciFiPanel.gameObject.SetActive (true);
		TempSciFiPanel.GetComponent<SciFiEquationManager>().ToggleVisuals(true);

		WhiteboardTopic tempTopic = Topic [topic];

		//Set Current Topic
		currentTopic = tempTopic;

		//Show Topic On Whiteboard as text
		WhiteBoardTitleTextRef.text =  tempTopic .Title;
		WhiteBoardTextRef.text = tempTopic .textForWhiteBoard;

		//Speak the whole topic
		#if UNITY_EDITOR
//		SpeechTextRef.Text = tempTopic .TextForNarrator;
//		SpeechTextRef.Speak ();
//		TTSSpeakerRef.Speak(tempTopic.TextForNarrator);
		#endif
	}




	void OnTopicNarrationComplete(SpeakEventArgs e){
		//Stop AstroyBoy's Speak Animation and go to idle state
		AstroBoyRef.SetTrigger("Idle");

		//If is last topic then 
		if(currentTopic.IsLastTopic){
			OnSceneTopicsComplete ();
			//Show Scene Complete Menu
			return;
		}

		//		Debug.LogError ("Speech Complete for" + currentTopic.Title );
		if (currentTopic.CompletesGameStateOnFinish)
			GameObject.FindObjectOfType<SceneFlowManager> ().NextState ();
		else
			ShowNextButton();



		//Increment To next Topic
		CurrentTopicIndex++;
	}



	void OnSceneTopicsComplete(){
		CurrentTopicIndex = GameStateInitial;
		Globals.CurrentState = (Globals.TorqueSceneGameState)CurrentTopicIndex;
		Messenger.Broadcast(Events.OnSceneTopicsComplete);
	}


	void OnTopicNarrationComplete(){
		//Stop AstroyBoy's Speak Animation and go to idle state
		AstroBoyRef.SetTrigger("Idle");

		//If is last topic then 
		if(currentTopic.IsLastTopic){
			OnSceneTopicsComplete ();
			//Show Scene Complete Menu
//			Messenger.Broadcast(Events.OnSceneTopicsComplete);
			return;
		}
		//		Debug.LogError ("Speech Complete for" + currentTopic.Title );
		if (currentTopic.CompletesGameStateOnFinish)
			GameObject.FindObjectOfType<SceneFlowManager> ().NextState ();
		else
			ShowNextButton();



		//Increment To next Topic
		CurrentTopicIndex++;
	}

	void OnTopicNarrationStart(SpeakEventArgs e){
		HideNextButton ();
		//Start AstroyBoy's Speak Animation and go to idle state
		AstroBoyRef.SetTrigger("Talk");
		//		AstroBoyRef.SetBool("IsTalking" , true);
	}


	void OnTopicNarrationStart(){
		HideNextButton ();
		//Start AstroyBoy's Speak Animation and go to idle state
		AstroBoyRef.SetTrigger("Talk");
		//		AstroBoyRef.SetBool("IsTalking" , true);
	}
}
