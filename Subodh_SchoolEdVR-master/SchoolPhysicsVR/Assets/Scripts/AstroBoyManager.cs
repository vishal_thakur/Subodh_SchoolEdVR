﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AstroBoyManager : MonoBehaviour {

	Animator AnimatorRef;
	[SerializeField] GameObject PlayerRef;


	[SerializeField] GameObject InteractionCanvasRef;

	[SerializeField] Text InteractionCanvasTextRef;

	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start(){
		AnimatorRef = GetComponent<Animator> ();

		Messenger.AddListener<Globals.AstroBoyAnimations>(Events.OnAstroBoyAnimationStateChanged , OnAnimationStateChanged);


		//Listen To Hint object Selected Event
		Messenger.AddListener<HintObject> (Events.OnHintObjectClicked , OnHintObjectClicked);	


		Messenger.AddListener<Globals.GameModes> (Events.OnGameModeChanged , OnGameModeChanged);
	}





	public void ShowInteractionUI(string msg){
		InteractionCanvasRef.SetActive (true);
		InteractionCanvasTextRef.text = msg;
	}

	void OnGameModeChanged(Globals.GameModes gameMode){
		switch (gameMode) {
		case Globals.GameModes.Help:
			InteractionCanvasRef.SetActive (false);
			break;
		case Globals.GameModes.Normal:
			InteractionCanvasRef.SetActive (true);
			break;
		}
	}



	/// <summary>
	/// Raises the animation state changed event.
	/// </summary>
	/// <param name="state">State.</param>
	void OnAnimationStateChanged(Globals.AstroBoyAnimations state){
		switch (state) {

		case Globals.AstroBoyAnimations.Idle:
			AnimatorRef.SetTrigger ("Idle");
			break;

		case Globals.AstroBoyAnimations.Talk:
			AnimatorRef.SetTrigger ("Talk");
			break;
		}
	}

//
//
//	void Update(){
//		transform.LookAt (PlayerRef.transform);
//	}




	//Handle Hint Object selected Callback
	void OnHintObjectClicked(HintObject hintObject){
		//Reposition To the Specified Hint Object's Position
		transform.position = new Vector3(hintObject.DesiredAstroBoyPosition.x, transform.position.y, hintObject.DesiredAstroBoyPosition.z);
	}

}