﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Globals{

	public static float TimeScale = 2;

	public static float InfoPointTimeout = 10;


	//Rope Params
	public static float MinRopeLength = 2.3f;
	public static float MaxRopeLength = 4.5f;
	public static float RopeExtensionSpeed = 0.1f;

	public static bool CanTeleport = true;

	public static string LiveText = "Nothing!";

	public static bool ShowAngles = true;


	public enum Topic{
		MechanicalEquilibrium = 0,
		Torque,
		TorqueMathematicalTreatment,
		RotationalEquilibrium
	}

	public static TorqueSceneGameState CurrentState = TorqueSceneGameState.Welcome;


	public enum TorqueSceneGameState{
		Welcome = 0,
		ExplainTorque,
		ShowTorqueExample,
		GiveChanceToOpenDoor,
		ExplainDoorOpenTorque,
		GiveChanceToCloseDoor,
		ExplainDoorCloseTorque,
		OpenAndCloseDoorCoupleOfTimes,
		ConcludeTorque,
		ExplainHanginFrameExample,
		FindTensionOfRopes,
		ShowTranslationEquilibriumEquationTreatment,
		ShowRotationalEquilibriumEquationTreatment,
		ShowSolvingOfRotationalAndTranslationEquilibriumEquations,
		InteractAndChangeXandYOfHangingFrame,
		ExplainRopeLengthVaryEffectsOnSystem,
		ExplainMechanicalEquilibrium,
		ChapterComplete,

	}


	public static GameModes CurrentGameMode = GameModes.Normal;


	public enum HelpOptions{
		Robot,
		Floor
	}

	public enum GameModes{
		Normal = 0,
		Help
	}

	public enum MainMenuOptions{
		Tutorial = 0,
		Start,
		Exit,
		About,
		Back,
		TopicTorque,
		TopicEquilibrium,
		MainMenu
	}

	public enum AstroBoyAnimations{
		Idle = 0,
		Talk
	}
}