﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour {
	

	// Use this for initialization
	void Start () {
		Messenger.AddListener (Events.OnControllerKeyDown , OnControllerClick);
	}


	void OnControllerClick(){
		
	}



	public void OnOptionSelected(MainMenuOption option){
//		Debug.LogError ("Main Menu Option Selected\t" + option.option.ToString ());
	
		//Do Desited Action upon a main menu button Load
		switch(option.option){
		case Globals.MainMenuOptions.About:
			//Show About info Panel
			break;
		case Globals.MainMenuOptions.Exit:
			//Exit Game
			Application.Quit ();
			break;
		case Globals.MainMenuOptions.TopicTorque:
			SceneManager.LoadScene ("Torque");
			break;

		case Globals.MainMenuOptions.TopicEquilibrium:
			SceneManager.LoadScene ("Equilibrium");
			//Show Topics Panel
			break;
		case Globals.MainMenuOptions.Tutorial:
			SceneManager.LoadScene ("Tutorial");
			//Load Tutorials Scene
			break;
		case Globals.MainMenuOptions.MainMenu:
			SceneManager.LoadScene ("MainMenu");
			//Load Tutorials Scene
			break;
		case Globals.MainMenuOptions.Back:
			//Load Maine Menu Panel

			break;
		}
	}
	// Update is called once per frame
	void Update () {
		
	}
}
