﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneFlowManager : MonoBehaviour {

	[SerializeField] TorqueManager TorqueExampleSetupObj;
	[SerializeField] StaticEquilibriumManager StaticEquilibriumExampleSetupObj;
	[SerializeField] WhiteboardManager WhiteboardManagerRef;
	[SerializeField] AstroBoyManager AstroBoyManagerRef;

	[SerializeField] Globals.TorqueSceneGameState InitialGameState;

	void Start(){

		Globals.CurrentState = InitialGameState;

		//Set App TimeScale
		Time.timeScale = Globals.TimeScale;

		Messenger.AddListener <bool>(Events.OnPaused , OnGamePaused);
	}



	/// <summary>
	/// Raises the game paused event.
	/// </summary>
	void OnGamePaused(bool flag){
		if(flag)
			Time.timeScale = 0;
		else
			Time.timeScale = Globals.TimeScale;
//		Debug.LogError ("freeze");
	}



//	public string LiveTextData = "Nothing";

//	void OnGUI(){
//		GUILayout.TextField (Globals.CurrentState.ToString());
////		if (Globals.LiveText)
//	}

	void OnGUI(){
		GUILayout.TextField(Globals.CurrentState.ToString());
		GUILayout.TextField(Globals.CanTeleport.ToString());
	}
////
	public void OnStateComplete(Globals.TorqueSceneGameState state){
//		Debug.LogError ("Called" + Time.time);
		switch (state) {
		case Globals.TorqueSceneGameState.Welcome:
			//Hide Narrator Interaction UI
//			AstroBoyManagerRef.HideInteractionUI();

			//Show WhiteBoard UI
			WhiteboardManagerRef.ShowCanvas();
			break;

		case Globals.TorqueSceneGameState.ExplainMechanicalEquilibrium:
			break;

		case Globals.TorqueSceneGameState.ExplainTorque:
			break;

		case Globals.TorqueSceneGameState.ShowTorqueExample:
//			TorqueExampleSetupObj.gameObject.SetActive (false);
			break;

		case Globals.TorqueSceneGameState.GiveChanceToOpenDoor:
			WhiteboardManagerRef.NextTopic ();
			break;

		case Globals.TorqueSceneGameState.ExplainDoorOpenTorque:
			break;

		case Globals.TorqueSceneGameState.GiveChanceToCloseDoor:
			WhiteboardManagerRef.NextTopic ();
			break;

		case Globals.TorqueSceneGameState.ExplainDoorCloseTorque:
			break;

		case Globals.TorqueSceneGameState.OpenAndCloseDoorCoupleOfTimes:
			TorqueExampleSetupObj.gameObject.SetActive (false);
			//			AstroBoyManagerRef.HideInteractionUI ();
			break;

		case Globals.TorqueSceneGameState.ConcludeTorque:
			WhiteboardManagerRef.NextTopic ();
			break;

		case Globals.TorqueSceneGameState.FindTensionOfRopes:
			WhiteboardManagerRef.NextTopic ();
			break;

		case Globals.TorqueSceneGameState.ShowTranslationEquilibriumEquationTreatment:
			WhiteboardManagerRef.NextTopic ();
			break;

		case Globals.TorqueSceneGameState.ShowRotationalEquilibriumEquationTreatment:
			WhiteboardManagerRef.NextTopic ();
			break;

		case Globals.TorqueSceneGameState.ShowSolvingOfRotationalAndTranslationEquilibriumEquations:
			WhiteboardManagerRef.NextTopic ();
			break;

		case Globals.TorqueSceneGameState.InteractAndChangeXandYOfHangingFrame:
			WhiteboardManagerRef.ToggleTypeWritereffect (true);
			WhiteboardManagerRef.ShowLiveText = false;
			break;

		case Globals.TorqueSceneGameState.ExplainRopeLengthVaryEffectsOnSystem:
			WhiteboardManagerRef.ShowLiveText = false;
			WhiteboardManagerRef.ToggleTypeWritereffect (true);
			Globals.ShowAngles = false;
			break;

		case Globals.TorqueSceneGameState.ChapterComplete:
			break;
		}


//		NextState ();
	}


	public void OnStateBegin(Globals.TorqueSceneGameState state){
		switch (state) {

		case Globals.TorqueSceneGameState.Welcome:
//			AstroBoyManagerRef.ShowInteractionUI ("Begin");
			break;

		case Globals.TorqueSceneGameState.ExplainMechanicalEquilibrium:
			WhiteboardManagerRef.NextTopic ();
			break;

		case Globals.TorqueSceneGameState.ExplainTorque:
			WhiteboardManagerRef.NextTopic ();
			break;

		case Globals.TorqueSceneGameState.ShowTorqueExample:
			TorqueExampleSetupObj.gameObject.SetActive (true);
			WhiteboardManagerRef.NextTopic ();
			break;

		case Globals.TorqueSceneGameState.GiveChanceToOpenDoor:
			TorqueExampleSetupObj.Initialize ();
			break;

		case Globals.TorqueSceneGameState.ExplainDoorOpenTorque:
			break;

		case Globals.TorqueSceneGameState.GiveChanceToCloseDoor:
			break;

		case Globals.TorqueSceneGameState.ExplainDoorCloseTorque:
			break;

		case Globals.TorqueSceneGameState.OpenAndCloseDoorCoupleOfTimes:
			AstroBoyManagerRef.ShowInteractionUI ("Proceed");
			break;

		case Globals.TorqueSceneGameState.ConcludeTorque:
			WhiteboardManagerRef.NextTopic ();
			break;

		case Globals.TorqueSceneGameState.ExplainHanginFrameExample:
			WhiteboardManagerRef.NextTopic ();
			StaticEquilibriumExampleSetupObj.gameObject.SetActive (true);
			//Disable interaction For now
			StaticEquilibriumExampleSetupObj.IsInteractible = false;
			break;


		case Globals.TorqueSceneGameState.FindTensionOfRopes:
			WhiteboardManagerRef.NextTopic ();
			break;

		case Globals.TorqueSceneGameState.ShowTranslationEquilibriumEquationTreatment:
			break;

		case Globals.TorqueSceneGameState.ShowRotationalEquilibriumEquationTreatment:
			break;

		case Globals.TorqueSceneGameState.ShowSolvingOfRotationalAndTranslationEquilibriumEquations:
			break;

		case Globals.TorqueSceneGameState.InteractAndChangeXandYOfHangingFrame:
			StaticEquilibriumExampleSetupObj.IsInteractible = true;
			WhiteboardManagerRef.ToggleTypeWritereffect (false);
			WhiteboardManagerRef.ShowLiveText = true;
			break;

		case Globals.TorqueSceneGameState.ExplainRopeLengthVaryEffectsOnSystem:
			WhiteboardManagerRef.NextTopic ();
			WhiteboardManagerRef.ToggleTypeWritereffect (false);
			WhiteboardManagerRef.ShowLiveText = true;
			Globals.ShowAngles = true;
			break;

		case Globals.TorqueSceneGameState.ChapterComplete:
			WhiteboardManagerRef.NextTopic ();
			StaticEquilibriumExampleSetupObj.gameObject.SetActive (false);
			break;
		}
	}





	public void NextState(){
		StartCoroutine (NextStateTransition());
	}

	IEnumerator NextStateTransition(){

		OnStateComplete (Globals.CurrentState);

		int currentState = (int)Globals.CurrentState;

		currentState++;

		Globals.CurrentState = (Globals.TorqueSceneGameState)currentState;

		yield return new WaitForEndOfFrame();

		OnStateBegin (Globals.CurrentState);
	}

//	public void SkipCurrentState(){
//		OnStateComplete (Globals.CurrentState);
//
//		int currentState = (int)Globals.CurrentState;
//
//		currentState++;
//
//		Globals.CurrentState = (Globals.TorqueSceneGameState)currentState;
//
//	}
}
