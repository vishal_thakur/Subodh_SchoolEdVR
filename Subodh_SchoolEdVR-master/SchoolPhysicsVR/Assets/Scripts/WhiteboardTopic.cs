﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WhiteboardTopic {


	public string Title;

//	[SerializeField] Globals.Topic topic;

	[Multiline(15)]
	public string textForWhiteBoard;

	[Multiline(15)]
	public string TextForNarrator;

	public bool CompletesGameStateOnFinish = false;
	public bool IsLastTopic = false;

	public AudioClip audio;

}
