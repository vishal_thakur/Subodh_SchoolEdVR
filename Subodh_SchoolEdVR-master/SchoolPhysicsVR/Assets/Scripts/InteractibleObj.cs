﻿// Copyright 2014 Google Inc. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

namespace GoogleVR.HelloVR {
	using UnityEngine;
	using System.Collections;
	using UnityEngine.SceneManagement;

	[RequireComponent(typeof(Collider))]
	public class InteractibleObj : MonoBehaviour {
		private Vector3 startingPosition;
		private Renderer renderer;

		public Material inactiveMaterial;
		public Material gazedAtMaterial;
//		[SerializeField] UltimateRope ConnectedRopeRef;	 

		//		[SerializeField] Vector2 HorizontalMovementMinMaxContraints;
		//		[SerializeField] Vector2 VerticalMovementMinMaxContraints;
//		[SerializeField] public bool CanMove = false;
//		[SerializeField] float MoveSpeed;
//		[SerializeField] StaticEquilibriumManager.AvailableHooks HookType;
//		LineRenderer LineRendererRef; 
//		[SerializeField]public Transform HangingObj;
		//		void OnGUI(){
		//			GUILayout.TextField (Input.GetAxis ("Horizontal").ToString());
		//			GUILayout.TextField (Input.GetAxis ("Vertical").ToString());
		//		}


		public void OnSelected(){
			Debug.LogError (gameObject.name + "\tSElected!");
//			StaticEquilibriumManager.pInstance.OnHookSelected (HookType);
		}

		void Start() {
			//      startingPosition = transform.localPosition;
			renderer = GetComponent<Renderer>();
			SetGazedAt(false);
		}


		void Update(){
//			//Draw Lines To Show Angles
//			LineRendererRef.SetPosition (0, transform.position);
//			LineRendererRef.SetPosition (1, HangingObj.position);
//
//			LineRendererRef.enabled = StaticEquilibriumManager.pInstance.isDiagramViewActive;
//
//			//Diagram View Toggle
//			//			GetComponent<MeshRenderer>().enabled = !StaticEquilibriumManager.pInstance.isDiagramViewActive;
//			HangingObj.GetChild(0).GetComponent<SkinnedMeshRenderer>().enabled = !StaticEquilibriumManager.pInstance.isDiagramViewActive;
//
//			if (CanMove) {
//
//				//Horizontal Joystick Movement
//				if (HookType == StaticEquilibriumManager.AvailableHooks.Left) {
//					if (Input.GetAxis ("Horizontal") < 0 && StaticEquilibriumManager.pInstance.CanGoAway)//move left
//						transform.position = Vector3.Lerp (transform.position, (new Vector3 ((transform.position.x - MoveSpeed), transform.position.y, transform.position.z)), Time.deltaTime * 2);
//					else if (Input.GetAxis ("Horizontal") > 0 && StaticEquilibriumManager.pInstance.CanComeClose)//move right
//						transform.position = Vector3.Lerp (transform.position, (new Vector3 ((transform.position.x + MoveSpeed), transform.position.y, transform.position.z)), Time.deltaTime * 2);
//				} else if (HookType == StaticEquilibriumManager.AvailableHooks.Right) {
//					if (Input.GetAxis ("Horizontal") < 0 && StaticEquilibriumManager.pInstance.CanComeClose)//move left
//						transform.position = Vector3.Lerp (transform.position, (new Vector3 ((transform.position.x - MoveSpeed), transform.position.y, transform.position.z)), Time.deltaTime * 2);
//					else if (Input.GetAxis ("Horizontal") > 0 && StaticEquilibriumManager.pInstance.CanGoAway)//move right
//						transform.position = Vector3.Lerp (transform.position, (new Vector3 ((transform.position.x + MoveSpeed), transform.position.y, transform.position.z)), Time.deltaTime * 2);
//				}

				//
				//				//Vertical Joystick Movement
				//				if(Input.GetAxis ("Vertical") < 0 &&  StaticEquilibriumManager.pInstance.CanGoAway)//move Down
				//					transform.position = Vector3.Lerp(transform.position , (new Vector3 (transform.position.x , transform.position.y - MoveSpeed , transform.position.z)) , Time.deltaTime * 2);
				//				else if(Input.GetAxis ("Vertical") > 0 &&  StaticEquilibriumManager.pInstance.CanGoAway)//move up
				//					transform.position = Vector3.Lerp(transform.position , (new Vector3 (transform.position.x, transform.position.y + MoveSpeed , transform.position.z)) , Time.deltaTime * 2);

//
//				//Vertical Joystick Movement
//				if (Input.GetAxis ("Vertical") < 0){//move Down{
//					//					Debug.LogError(ConnectedRopeRef.RopeNodes[0].fLength.ToString());
//					ConnectedRopeRef.RopeNodes[0].fLength = Mathf.Lerp (ConnectedRopeRef.RopeNodes[0].fLength ,Mathf.Max(Globals.MinRopeLength , ConnectedRopeRef.RopeNodes[0].fLength-=0.5f) , Time.deltaTime);
//					ConnectedRopeRef.Regenerate ();
//				}
//				else if (Input.GetAxis ("Vertical") > 0){//move Up{
//					//					Debug.LogError(ConnectedRopeRef.RopeNodes[0].fLength.ToString());
//					ConnectedRopeRef.RopeNodes[0].fLength = Mathf.Lerp (ConnectedRopeRef.RopeNodes[0].fLength ,Mathf.Min(Globals.MaxRopeLength , ConnectedRopeRef.RopeNodes[0].fLength+=0.5f) , Time.deltaTime);
//					ConnectedRopeRef.Regenerate ();
//				}
//			}

		}

		public void SetGazedAt(bool gazedAt) {
//			Debug.LogError ("Gazed at" + gameObject.name + gazedAt);
			if (inactiveMaterial != null && gazedAtMaterial != null) {
				renderer.material = gazedAt ? gazedAtMaterial : inactiveMaterial;
				return;
			}
		}

		public void Reset() {
//			int sibIdx = transform.GetSiblingIndex();
//			int numSibs = transform.parent.childCount;
//			for (int i=0; i<numSibs; i++) {
//				GameObject sib = transform.parent.GetChild(i).gameObject;
//				sib.transform.localPosition = startingPosition;
//				sib.SetActive(i == sibIdx);
//			}
		}

		public void Recenter() {
//			#if !UNITY_EDITOR
//			GvrCardboardHelpers.Recenter();
//			#else
//			if (GvrEditorEmulator.Instance != null) {
//				GvrEditorEmulator.Instance.Recenter();
//			}
//			#endif  // !UNITY_EDITOR
		}

		public void TeleportRandomly() {
//			return;
//			// Pick a random sibling, move them somewhere random, activate them,
//			// deactivate ourself.
//			int sibIdx = transform.GetSiblingIndex();
//			int numSibs = transform.parent.childCount;
//			sibIdx = (sibIdx + Random.Range(1, numSibs)) % numSibs;
//			GameObject randomSib = transform.parent.GetChild(sibIdx).gameObject;
//
//			// Move to random new location ±100º horzontal.
//			Vector3 direction = Quaternion.Euler(
//				0,
//				Random.Range(-90, 90),
//				0) * Vector3.forward;
//			// New location between 1.5m and 3.5m.
//			float distance = 2 * Random.value + 1.5f;
//			Vector3 newPos = direction * distance;
//			// Limit vertical position to be fully in the room.
//			newPos.y = Mathf.Clamp(newPos.y, -1.2f, 4f);
//			randomSib.transform.localPosition = newPos;
//
//			randomSib.SetActive(true);
//			gameObject.SetActive(false);
//			SetGazedAt(false);
		}
	}
}
