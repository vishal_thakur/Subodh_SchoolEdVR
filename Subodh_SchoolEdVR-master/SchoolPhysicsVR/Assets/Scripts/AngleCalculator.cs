﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AngleCalculator : MonoBehaviour {

	[SerializeField] Transform target;
	[SerializeField] Transform AnotherCube;

	[SerializeField] Image AngleImageRef;
	[SerializeField] Text AngleTextRef;
	[SerializeField] public float angle;

//	[SerializeField] Direction DirectionName;
	[SerializeField] Color angleArcColor;
	[SerializeField] Color angleTextColor;
//    Vector3 AngleDirection; 


	public int Fixedangle = 0;
//	enum Direction{
//		Up,
//		Down,
//		Left,
//		Right,
//		Forward
//	}


//	[SerializeField]

	void Start(){
		AngleImageRef.color = angleArcColor;
		AngleTextRef.color = angleTextColor;
	}

	void Update()
	{
		if (!Globals.ShowAngles) {
			AngleImageRef.enabled = false;
			AngleTextRef.enabled = false;
		} else {
			AngleImageRef.enabled = true;
			AngleTextRef.enabled = true;
		}


		Vector3 targetDirToHook = target.localPosition - transform.localPosition;
		Vector3 targetDirToOtherCube = AnotherCube.localPosition - transform.localPosition;

		angle = Vector3.Angle(targetDirToHook, targetDirToOtherCube);

//		if(hook)
		AngleTextRef.text = Fixedangle + " °";

		AngleImageRef.fillAmount = Mathf.Lerp(AngleImageRef.fillAmount , (angle / 360f), 10f) ;

//		if (angle < 5.0f)
//			print("close");
	}
}
