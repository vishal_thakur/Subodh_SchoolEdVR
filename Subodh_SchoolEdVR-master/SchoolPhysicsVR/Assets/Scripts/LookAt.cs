﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAt : MonoBehaviour {

	[SerializeField]
	Transform Target;

	[SerializeField]
	bool Followx;
	[SerializeField]
	bool Followy;
	[SerializeField]
	bool Followz;

	[SerializeField]
	float damping;

	// Update is called once per frame
	void Update () {

		Vector3 lookPos = Target.position - transform.position;

		if(Followx)
			lookPos.x = 0;
		if(Followy)
			lookPos.y = 0;
		if(Followz)
			lookPos.z = 0;

		Quaternion rotation = Quaternion.LookRotation (lookPos);

		transform.rotation = Quaternion.Slerp (transform.rotation , rotation , Time.deltaTime * damping);
	}
}
