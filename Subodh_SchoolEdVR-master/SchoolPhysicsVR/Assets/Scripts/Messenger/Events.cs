﻿using System;

public class Events{


	public static string OnControllerKeyDown = "OnControllerKeyDown";

	public static string OnHintObjectClicked = "OnHintObjectClicked";

	public static string OnGameModeChanged = "OnGameModeChanged";

	public static string OnPaused = "OnPaused";

	public static string OnSceneTopicsComplete = "OnSceneTopicsComplete";

	public static string OnAstroBoyAnimationStateChanged = "OnAstroBoyAnimationStateChanged";




}