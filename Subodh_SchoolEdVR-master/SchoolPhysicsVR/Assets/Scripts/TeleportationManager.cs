﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Crosstales.RTVoice.Tool;

public class TeleportationManager : MonoBehaviour {

	[SerializeField] LayerMask LayerMashSettings; 
	[SerializeField] Camera CameraRef;
	[SerializeField] Transform TeleportationDestinationObj;
	public bool TeleportationPossible{ get; set;}
//	public bool ExternalCauseTeleportationPossible = true;
	// Use this for initialization
	void Start () {
		Messenger.AddListener (Events.OnControllerKeyDown , OnControllerClick);
		CameraRef = GetComponentInChildren<Camera> ();
//		#if CT_RTV
//		Debug.LogError(“RTV installed: “ + Util.Constants.ASSET_VERSION);
//		#else
//		Debug.LogError("RTV NOT installed!");
//		#endif
	}


	void OnControllerClick(){
		if (Globals.CanTeleport) 
			Teleport ();
	}
	
	// Update is called once per frame
	void LateUpdate () {
		if (Globals.CanTeleport) {
		
//		Debug.DrawRay (CameraRef.transform.position , transform.forward, Color.red);

			RaycastHit hit;
//			Ray ray = positionto

			if (Physics.Raycast (CameraRef.transform.position, CameraRef.transform.forward, out hit, 100, LayerMashSettings)) {
//				Debug.LogError (hit.collider.gameObject.name + "\t" + hit.collider.gameObject.layer.ToString());

				if (hit.collider.gameObject.layer != 10)
					return;
				
				TeleportationPossible = true;
				Transform objectHit = hit.transform;
				
				TeleportationDestinationObj.position = hit.point;
				// Do something with the object that was hit by the raycast.
				if (Input.GetButton ("Submit") || Input.GetMouseButtonUp (0))
					Teleport ();

			} else {
				TeleportationDestinationObj.position = new Vector3 (1000, 1000, 1000);
				TeleportationPossible = false;
			}
		} else {
			TeleportationPossible = false;
			TeleportationDestinationObj.position = new Vector3 (1000, 1000, 1000);
		}
		
	}


	void Teleport(){
		if (TeleportationPossible) {
			transform.position = new Vector3 (TeleportationDestinationObj.position.x, 				
				transform.position.y, TeleportationDestinationObj.position.z);
		}
	}
		
}
