﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverManager : MonoBehaviour {


	[SerializeField] Transform SpawnPointInPlayerFOV;

	[SerializeField] GameObject GameOverMenuCanvasRef;

	[SerializeField]
	GameObject GameVisuals;



	void Start(){
		Messenger.AddListener (Events.OnSceneTopicsComplete, OnSceneTopicsComplete);

		GameOverMenuCanvasRef.SetActive (false);
	}



	void OnSceneTopicsComplete(){
		GameVisuals.SetActive (false);
		GameOverMenuCanvasRef.SetActive (true);
		GameOverMenuCanvasRef.transform.position = SpawnPointInPlayerFOV.position;
	}


	public void OnReplay(){
		//ReLoad Current Topic Scene
		SceneManager.LoadScene (SceneManager.GetActiveScene().name);
			
	}
	public void OnMainMenu(){
		//Load Main Menu Scene
		SceneManager.LoadScene ("MainMenu");
	}
}
