// Copyright 2014 Google Inc. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

namespace GoogleVR.HelloVR {
	using UnityEngine;
	using System.Collections;
	using UnityEngine.SceneManagement;

  [RequireComponent(typeof(Collider))]
  public class ObjectController : MonoBehaviour {
	    private Vector3 startingPosition;
	    private Renderer renderer;
		bool isGazedAt = false;

    	public Material inactiveMaterial;

		public Material gazedAtMaterial;
		[SerializeField] UltimateRope ConnectedRopeRef;	 

		float m_fRopeExtension = 0;
		[SerializeField] public bool CanMove = false;
		[SerializeField] float MoveSpeed;
		[SerializeField] StaticEquilibriumManager.AvailableHooks HookType;
		LineRenderer LineRendererRef; 

		//X is max and Y is min
		[SerializeField] Vector2 MinMaxMovementValues;

		//configurable Joint Params
		[Space(10) ,Header("Configurable Joint Params")] 
		[SerializeField]public FixedJoint BottomHookJointRef; 

		//X is max and Y is min
		[SerializeField] Vector2 MinMaxAnchorValues;

		//Move Speed
		[SerializeField] float BottomHookMoveSpeed;


		public void OnSelected(){
			if(isGazedAt)
				StaticEquilibriumManager.pInstance.OnHookSelected (HookType);
		}

    
		void Start() {


			LineRendererRef = GetComponent<LineRenderer> ();
	      renderer = GetComponent<Renderer>();
	      SetGazedAt(false);

			Messenger.AddListener (Events.OnControllerKeyDown, OnSelected);
	    }



		void Update(){
			//Draw Lines To Show Angles
			LineRendererRef.SetPosition (0, BottomHookJointRef.transform.position);
			LineRendererRef.SetPosition (1, transform.position);

			LineRendererRef.enabled = StaticEquilibriumManager.pInstance.isDiagramViewActive;

			//Diagram View Toggle
//			HangingObj.GetComponent<MeshRenderer>().enabled = !StaticEquilibriumManager.pInstance.isDiagramViewActive;






			if (CanMove) {
				//Hack to Render The Joint Movement WTF!! Why you do this Unity ?
//				BottomHookJointRef.configuredInWorldSpace = false;
				//Horizontal Joystick Movement | Left Hook
				if (HookType == StaticEquilibriumManager.AvailableHooks.Left) {
					if (Input.GetAxis ("Horizontal") < 0 && StaticEquilibriumManager.pInstance.CanGoAway) {//move left
//						BottomHookJointRef.connectedAnchor = new Vector3 (Mathf.Clamp(BottomHookJointRef.connectedAnchor.x - (Time.deltaTime * BottomHookMoveSpeed) , MinMaxAnchorValues.x , MinMaxAnchorValues.y) , BottomHookJointRef.connectedAnchor.y , BottomHookJointRef.connectedAnchor.z);
						transform.localPosition = new Vector3 (Mathf.Clamp (transform.localPosition.x - (Time.deltaTime * MoveSpeed), MinMaxMovementValues.x, MinMaxMovementValues.y), transform.localPosition.y, transform.localPosition.z);
//						transform.Translate (Vector3.left * MoveSpeed * Time.deltaTime);	
					}
//					transform.position = Vector3.Lerp (transform.position, (new Vector3 ((transform.position.x - MoveSpeed), transform.position.y, transform.position.z)), Time.deltaTime * 2);
					else if (Input.GetAxis ("Horizontal") > 0 && StaticEquilibriumManager.pInstance.CanComeClose) {//move right
//						BottomHookJointRef.connectedAnchor = new Vector3(Mathf.Clamp(BottomHookJointRef.connectedAnchor.x + (Time.deltaTime * BottomHookMoveSpeed) , MinMaxAnchorValues.x , MinMaxAnchorValues.y) , BottomHookJointRef.connectedAnchor.y , BottomHookJointRef.connectedAnchor.z);
						transform.localPosition = new Vector3 (Mathf.Clamp (transform.localPosition.x + (Time.deltaTime * MoveSpeed), MinMaxMovementValues.x, MinMaxMovementValues.y), transform.localPosition.y, transform.localPosition.z);
//						transform.Translate (-Vector3.left * MoveSpeed * Time.deltaTime);	
					}
//					transform.position = Vector3.Lerp (transform.position, (new Vector3 ((transform.position.x + MoveSpeed), transform.position.y, transform.position.z)), Time.deltaTime * 2);
				}
				// Right Hook
				else if (HookType == StaticEquilibriumManager.AvailableHooks.Right) {
					if (Input.GetAxis ("Horizontal") < 0 && StaticEquilibriumManager.pInstance.CanComeClose) {//move left
//						BottomHookJointRef.connectedAnchor = new Vector3 (Mathf.Clamp (BottomHookJointRef.connectedAnchor.x - (Time.deltaTime * BottomHookMoveSpeed) , MinMaxAnchorValues.x, MinMaxAnchorValues.y) , BottomHookJointRef.connectedAnchor.y , BottomHookJointRef.connectedAnchor.z);
						transform.localPosition = new Vector3 (Mathf.Clamp (transform.localPosition.x - (Time.deltaTime * MoveSpeed), MinMaxMovementValues.x, MinMaxMovementValues.y), transform.localPosition.y, transform.localPosition.z);
//						transform.Translate (Vector3.left * MoveSpeed * Time.deltaTime);	
					}
//						transform.position = Vector3.Lerp (transform.position, (new Vector3 ((transform.position.x - MoveSpeed), transform.position.y, transform.position.z)), Time.deltaTime * 2);
					else if (Input.GetAxis ("Horizontal") > 0 && StaticEquilibriumManager.pInstance.CanGoAway) {//move right
//						BottomHookJointRef.connectedAnchor = new Vector3 (Mathf.Clamp(BottomHookJointRef.connectedAnchor.x + (Time.deltaTime * BottomHookMoveSpeed) , MinMaxAnchorValues.x , MinMaxAnchorValues.y) , BottomHookJointRef.connectedAnchor.y , BottomHookJointRef.connectedAnchor.z);
						transform.localPosition = new Vector3 (Mathf.Clamp (transform.localPosition.x + (Time.deltaTime * MoveSpeed), MinMaxMovementValues.x, MinMaxMovementValues.y), transform.localPosition.y, transform.localPosition.z);
//						transform.Translate (-Vector3.left * MoveSpeed * Time.deltaTime);	
					}
//						transform.position = Vector3.Lerp (transform.position, (new Vector3 ((transform.position.x + MoveSpeed), transform.position.y, transform.position.z)), Time.deltaTime * 2);
				}



				//Update Top Hooks position with the Bottom Hooks Position
//				transform.position = new Vector3(BottomHookJointRef.transform.position.x,  transform.position.y , transform.position.z);

//
//				//Vertical Joystick Movement
//				if(Input.GetAxis ("Vertical") < 0 &&  StaticEquilibriumManager.pInstance.CanGoAway)//move Down
//					transform.position = Vector3.Lerp(transform.position , (new Vector3 (transform.position.x , transform.position.y - MoveSpeed , transform.position.z)) , Time.deltaTime * 2);
//				else if(Input.GetAxis ("Vertical") > 0 &&  StaticEquilibriumManager.pInstance.CanGoAway)//move up
//					transform.position = Vector3.Lerp(transform.position , (new Vector3 (transform.position.x, transform.position.y + MoveSpeed , transform.position.z)) , Time.deltaTime * 2);

//				ConnectedRopeRef.ExtendRope

				//Vertical Joystick Movement , Rope Length Alter
				if (Input.GetAxis ("Vertical") < 0){//move Down{
					m_fRopeExtension = Mathf.Clamp(m_fRopeExtension += Time.deltaTime *  Globals.RopeExtensionSpeed , 0 , ConnectedRopeRef.ExtensibleLength);
					ConnectedRopeRef.ExtendRope(UltimateRope.ERopeExtensionMode.LinearExtensionIncrement, m_fRopeExtension - ConnectedRopeRef.m_fCurrentExtension);
				}
				else if (Input.GetAxis ("Vertical") > 0){//move Up{
					m_fRopeExtension = Mathf.Clamp(m_fRopeExtension -= Time.deltaTime *  Globals.RopeExtensionSpeed , 0 , ConnectedRopeRef.ExtensibleLength);
					ConnectedRopeRef.ExtendRope(UltimateRope.ERopeExtensionMode.LinearExtensionIncrement, m_fRopeExtension - ConnectedRopeRef.m_fCurrentExtension);
				}
			}
		}

    public void SetGazedAt(bool gazedAt) {
			isGazedAt = gazedAt;
      if (inactiveMaterial != null && gazedAtMaterial != null) {
        renderer.material = gazedAt ? gazedAtMaterial : inactiveMaterial;
        return;
      }
    }

    public void Reset() {
      int sibIdx = transform.GetSiblingIndex();
      int numSibs = transform.parent.childCount;
      for (int i=0; i<numSibs; i++) {
        GameObject sib = transform.parent.GetChild(i).gameObject;
        sib.transform.localPosition = startingPosition;
        sib.SetActive(i == sibIdx);
      }
    }

    public void Recenter() {
#if !UNITY_EDITOR
      GvrCardboardHelpers.Recenter();
#else
      if (GvrEditorEmulator.Instance != null) {
        GvrEditorEmulator.Instance.Recenter();
      }
#endif  // !UNITY_EDITOR
    }

    public void TeleportRandomly() {
			return;
      // Pick a random sibling, move them somewhere random, activate them,
      // deactivate ourself.
      int sibIdx = transform.GetSiblingIndex();
      int numSibs = transform.parent.childCount;
      sibIdx = (sibIdx + Random.Range(1, numSibs)) % numSibs;
      GameObject randomSib = transform.parent.GetChild(sibIdx).gameObject;

      // Move to random new location ±100º horzontal.
      Vector3 direction = Quaternion.Euler(
          0,
          Random.Range(-90, 90),
          0) * Vector3.forward;
      // New location between 1.5m and 3.5m.
      float distance = 2 * Random.value + 1.5f;
      Vector3 newPos = direction * distance;
      // Limit vertical position to be fully in the room.
      newPos.y = Mathf.Clamp(newPos.y, -1.2f, 4f);
      randomSib.transform.localPosition = newPos;

      randomSib.SetActive(true);
      gameObject.SetActive(false);
      SetGazedAt(false);
    }
  }
}
