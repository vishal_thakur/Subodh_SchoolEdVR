using UnityEngine;
using System.Collections;

namespace Crosstales.RTVoice.Tool
{
    /// <summary>Checks the Internet availabilty.</summary>
    [ExecuteInEditMode]
    [DisallowMultipleComponent]
	[HelpURL("https://www.crosstales.com/media/data/assets/rtvoice/api/class_crosstales_1_1_r_t_voice_1_1_tool_1_1_internet_check.html")]
    public class InternetCheck : MonoBehaviour
    {

        #region Variables

        /// <summary>Optimized check routines (default: on).</summary>
        [Tooltip("Optimized check routines (default: on)")]
        public bool Optimized = true;

        /// <summary>Check interval minimum in seconds (default: 20).</summary>
        [Tooltip("Check interval minimum in seconds (default: 20)")]
        [Range(1f, 60f)]
        public float CheckIntervalMin = 20f;

        /// <summary>Check interval minimum in seconds (default: 40).</summary>
        [Tooltip("Check interval maximum in seconds (default: 40)")]
        [Range(1f, 60f)]
        public float CheckIntervalMax = 40f;

        private const bool runOnStart = true;
        private const bool endlessMode = true;

        private static bool internetAvailable = false;

        private static GameObject go;
        private static InternetCheck instance;
        private static bool initalized = false;
        private static bool loggedOnlyOneInstance = false;

        #endregion


        #region Properties

        /// <summary>Checks if a Internet connection is available.</summary>
        /// <returns>True if a Internet connection is available.</returns>
        public static bool isInternetAvailable
        {
            get
            {
                if (instance == null)
                {
                    return Application.internetReachability != NetworkReachability.NotReachable;
                }
                else
                {
                    return internetAvailable;
                }
            }
        }

        #endregion


        #region MonoBehaviour methods

        public void OnEnable()
        {
            if (Util.Helper.isEditorMode || !initalized)
            {
                go = gameObject;

                go.name = Util.Constants.INTERNETCHECK_SCENE_OBJECT_NAME;

                instance = this;

                if (runOnStart)
                {
                    StopAllCoroutines();
                    StartCoroutine(internetCheck());
                }

                if (!Util.Helper.isEditorMode && Util.Config.DONT_DESTROY_ON_LOAD)
                {
                    DontDestroyOnLoad(transform.root.gameObject);

                    initalized = true;
                }

                if (Util.Constants.DEV_DEBUG)
                    Debug.Log("Using new instance!");
            }
            else
            {
                if (!Util.Helper.isEditorMode && Util.Config.DONT_DESTROY_ON_LOAD)
                {
                    if (!loggedOnlyOneInstance)
                    {
                        Debug.LogWarning("Only one active instance of '" + Util.Constants.INTERNETCHECK_SCENE_OBJECT_NAME + "' allowed in all scenes!" + System.Environment.NewLine + "This object will now be destroyed.");

                        Destroy(gameObject, 0.2f);

                        loggedOnlyOneInstance = true;
                    }
                }

                if (Util.Constants.DEV_DEBUG)
                    Debug.Log("Using old instance!");
            }
        }

        public void Update()
        {
            if (Util.Helper.isEditorMode)
            {
                if (go != null)
                {
                    go.name = Util.Constants.INTERNETCHECK_SCENE_OBJECT_NAME; //ensure name
                }
            }
        }

        public void OnApplicationQuit()
        {
            if (instance != null)
            {
                instance.StopAllCoroutines();
            }
        }

        #endregion


        #region Public methods

        public static void Refresh()
        {
            if (instance != null)
            {
                //Debug.Log("Refresh: " + System.DateTime.Now);

                instance.StopAllCoroutines();
                instance.StartCoroutine(instance.internetCheck());
            }
        }

        #endregion


        #region Private methods

        private IEnumerator internetCheck()
        {
            WWW www;
            bool available;

            do
            {
                //Debug.Log("internetCheck: " + System.DateTime.Now);

                available = false;

                if (Util.Helper.isWebPlatform || Util.Helper.isEditorMode)
                {
                    available = Application.internetReachability != NetworkReachability.NotReachable;
                }
                else
                {
                    if (Optimized || Util.Helper.isWindowsBasedPlatform)
                    {
                        if (Util.Config.DEBUG)
                            Debug.Log("Testing the Internet availability for Optimized/Windows-based systems: " + System.DateTime.Now);

                        using (www = new WWW(Util.Constants.INTERNET_CHECK_URL_WINDOWS))
                        {
                            do
                            {
                                yield return www;
                            } while (!www.isDone);

                            if (string.IsNullOrEmpty(www.error))
                            {
                                if (Util.Constants.DEV_DEBUG)
                                    Debug.Log("Content for Windows-based systems: " + www.text);

                                available = !string.IsNullOrEmpty(www.text) && www.text.CTEquals("Microsoft NCSI");
                            }
                        }
                    }

                    if (!Optimized && Util.Helper.isAppleBasedPlatform)
                    {
                        if (Util.Config.DEBUG)
                            Debug.Log("Testing the Internet availability for Apple-based systems: " + System.DateTime.Now);

                        using (www = new WWW(Util.Constants.INTERNET_CHECK_URL_APPLE))
                        {
                            do
                            {
                                yield return www;
                            } while (!www.isDone);

                            if (string.IsNullOrEmpty(www.error))
                            {
                                if (Util.Constants.DEV_DEBUG)
                                    Debug.Log("Content for Apple-based systems: " + www.text);

                                available = !string.IsNullOrEmpty(www.text) && www.text.CTContains("<TITLE>Success</TITLE>");
                            }
                        }
                    }

                    // default check
                    if (!available)
                    {
                        if (Util.Config.DEBUG)
                            Debug.Log("Testing with default Internet availability: " + System.DateTime.Now);

                        using (www = new WWW(Util.Constants.INTERNET_CHECK_URL))
                        {
                            do
                            {
                                yield return www;
                            } while (!www.isDone);

                            if (string.IsNullOrEmpty(www.error))
                            {
                                if (Util.Constants.DEV_DEBUG)
                                    Debug.Log("Content for default: " + www.text);

                                available = !string.IsNullOrEmpty(www.text) && www.text.CTContains("<TITLE>Lorem Ipsum</TITLE>");
                            }
                        }
                    }
                }

                internetAvailable = available;


                if (endlessMode)
                {
                    yield return new WaitForSeconds(Random.Range(CheckIntervalMin, CheckIntervalMax));
                }

            } while (endlessMode);
        }

        #endregion

    }
}
// � 2017 crosstales LLC (https://www.crosstales.com)