﻿namespace Crosstales.RTVoice.Util
{
    /// <summary>Configuration for the asset.</summary>
    public static class Config
    {

        #region Changable variables

        /// <summary>Path to the asset inside the Unity project.</summary>
        public static string ASSET_PATH = Constants.DEFAULT_ASSET_PATH;

        /// <summary>Enable or disable debug logging for the asset.</summary>
        public static bool DEBUG = Constants.DEFAULT_DEBUG;

        /// <summaryEnable or disable update-checks for the asset.</summary>
        public static bool UPDATE_CHECK = Constants.DEFAULT_UPDATE_CHECK;

        /// <summaryOpen the UAS-site when an update is found.</summary>
        public static bool UPDATE_OPEN_UAS = Constants.DEFAULT_UPDATE_OPEN_UAS;

        /// <summary>Don't destroy the objects during scene switches.</summary>
        public static bool DONT_DESTROY_ON_LOAD = Constants.DEFAULT_DONT_DESTROY_ON_LOAD;

        /// <summary>Automatically load and add the prefabs to the scene.</summary>
        public static bool PREFAB_AUTOLOAD = Constants.DEFAULT_PREFAB_AUTOLOAD;

        /// <summary>Path to the generated audio files.</summary>
        public static string AUDIOFILE_PATH = Constants.DEFAULT_AUDIOFILE_PATH;

        /// <summary>Automatically delete the generated audio files.</summary>
        public static bool AUDIOFILE_AUTOMATIC_DELETE = Constants.DEFAULT_AUDIOFILE_AUTOMATIC_DELETE;

        /// <summary>Enable or disable the icon in the hierarchy.</summary>
        public static bool HIERARCHY_ICON = Constants.DEFAULT_HIERARCHY_ICON;

        /// <summary>Enforce 32bit versions of voices under Windows.</summary>
        public static bool ENFORCE_32BIT_WINDOWS = Constants.DEFAULT_ENFORCE_32BIT_WINDOWS;

        // Technical settings

        /// <summary>Location of the TTS-wrapper under Windows (stand-alone).</summary>
        public static string TTS_WINDOWS_BUILD = Constants.DEFAULT_TTS_WINDOWS_BUILD;

        /// <summary>Location of the TTS-system under MacOS.</summary>
        public static string TTS_MACOS = Constants.DEFAULT_TTS_MACOS;

        /// <summary>Kill processes after 3000 milliseconds.</summary>
        public static int TTS_KILL_TIME = Constants.DEFAULT_TTS_KILL_TIME;

        #endregion


        #region Properties

        /// <summary>Returns the path of the prefabs.</summary>
		/// <returns>The path of the prefabs.</returns>
        public static string PREFAB_PATH
        {
            get
            {
                return ASSET_PATH + Constants.PREFAB_SUBPATH;
            }
        }

        /// <summary>Location of the TTS-wrapper under Windows (Editor).</summary>
        public static string TTS_WINDOWS_EDITOR
        {
            get
            {
                return ASSET_PATH + Constants.TTS_WINDOWS_SUBPATH;
            }
        }

        /// <summary>Location of the TTS-wrapper (32bit) under Windows (Editor).</summary>
        public static string TTS_WINDOWS_EDITOR_x86
        {
            get
            {
                return ASSET_PATH + Constants.TTS_WINDOWS_x86_SUBPATH;
            }
        }

        #endregion


        #region Public static methods

        /// <summary>Resets all changable variables to their default value.</summary>
        public static void Reset()
        {
            ASSET_PATH = Constants.DEFAULT_ASSET_PATH;
            DEBUG = Constants.DEFAULT_DEBUG;
            UPDATE_CHECK = Constants.DEFAULT_UPDATE_CHECK;
            UPDATE_OPEN_UAS = Constants.DEFAULT_UPDATE_OPEN_UAS;
            DONT_DESTROY_ON_LOAD = Constants.DEFAULT_DONT_DESTROY_ON_LOAD;
            PREFAB_AUTOLOAD = Constants.DEFAULT_PREFAB_AUTOLOAD;
            AUDIOFILE_PATH = Constants.DEFAULT_AUDIOFILE_PATH;
            AUDIOFILE_AUTOMATIC_DELETE = Constants.DEFAULT_AUDIOFILE_AUTOMATIC_DELETE;
            HIERARCHY_ICON = Constants.DEFAULT_HIERARCHY_ICON;
            ENFORCE_32BIT_WINDOWS = Constants.DEFAULT_ENFORCE_32BIT_WINDOWS;
            TTS_WINDOWS_BUILD = Constants.DEFAULT_TTS_WINDOWS_BUILD;
            TTS_MACOS = Constants.DEFAULT_TTS_MACOS;
            TTS_KILL_TIME = Constants.DEFAULT_TTS_KILL_TIME;
        }

        /// <summary>Loads all changable variables.</summary>
        public static void Load()
        {
            if (CTPlayerPrefs.HasKey(Constants.KEY_DEBUG))
            {
                DEBUG = CTPlayerPrefs.GetBool(Constants.KEY_DEBUG);
            }

            if (CTPlayerPrefs.HasKey(Constants.KEY_UPDATE_CHECK))
            {
                UPDATE_CHECK = CTPlayerPrefs.GetBool(Constants.KEY_UPDATE_CHECK);
            }

            if (CTPlayerPrefs.HasKey(Constants.KEY_UPDATE_OPEN_UAS))
            {
                UPDATE_OPEN_UAS = CTPlayerPrefs.GetBool(Constants.KEY_UPDATE_OPEN_UAS);
            }

            //if (CTPlayerPrefs.HasKey(Constants.KEY_DONT_DESTROY_ON_LOAD))
            //{
            //    DONT_DESTROY_ON_LOAD = CTPlayerPrefs.GetBool(Constants.KEY_DONT_DESTROY_ON_LOAD);
            //}

            if (CTPlayerPrefs.HasKey(Constants.KEY_ASSET_PATH))
            {
                ASSET_PATH = CTPlayerPrefs.GetString(Constants.KEY_ASSET_PATH);
            }

            if (CTPlayerPrefs.HasKey(Constants.KEY_PREFAB_AUTOLOAD))
            {
                PREFAB_AUTOLOAD = CTPlayerPrefs.GetBool(Constants.KEY_PREFAB_AUTOLOAD);
            }

            if (CTPlayerPrefs.HasKey(Constants.KEY_AUDIOFILE_PATH))
            {
                AUDIOFILE_PATH = CTPlayerPrefs.GetString(Constants.KEY_AUDIOFILE_PATH);
            }

            if (CTPlayerPrefs.HasKey(Constants.KEY_AUDIOFILE_AUTOMATIC_DELETE))
            {
                AUDIOFILE_AUTOMATIC_DELETE = CTPlayerPrefs.GetBool(Constants.KEY_AUDIOFILE_AUTOMATIC_DELETE);
            }

            if (CTPlayerPrefs.HasKey(Constants.KEY_HIERARCHY_ICON))
            {
                HIERARCHY_ICON = CTPlayerPrefs.GetBool(Constants.KEY_HIERARCHY_ICON);
            }

            if (CTPlayerPrefs.HasKey(Constants.KEY_ENFORCE_32BIT_WINDOWS))
            {
                ENFORCE_32BIT_WINDOWS = CTPlayerPrefs.GetBool(Constants.KEY_ENFORCE_32BIT_WINDOWS);
            }

            //if (CTPlayerPrefs.HasKey(Constants.KEY_TTS_MACOS))
            //{
            //    TTS_MACOS = CTPlayerPrefs.GetString(Constants.KEY_TTS_MACOS);
            //}
        }

        /// <summary>Saves all changable variables.</summary>
        public static void Save()
        {
            CTPlayerPrefs.SetString(Constants.KEY_ASSET_PATH, ASSET_PATH);
            CTPlayerPrefs.SetBool(Constants.KEY_DEBUG, DEBUG);
            CTPlayerPrefs.SetBool(Constants.KEY_UPDATE_CHECK, UPDATE_CHECK);
            CTPlayerPrefs.SetBool(Constants.KEY_UPDATE_OPEN_UAS, UPDATE_OPEN_UAS);
            //CTPlayerPrefs.SetBool(Constants.KEY_DONT_DESTROY_ON_LOAD, DONT_DESTROY_ON_LOAD);
            CTPlayerPrefs.SetBool(Constants.KEY_PREFAB_AUTOLOAD, PREFAB_AUTOLOAD);
            CTPlayerPrefs.SetString(Constants.KEY_AUDIOFILE_PATH, AUDIOFILE_PATH);
            CTPlayerPrefs.SetBool(Constants.KEY_AUDIOFILE_AUTOMATIC_DELETE, AUDIOFILE_AUTOMATIC_DELETE);
            CTPlayerPrefs.SetBool(Constants.KEY_HIERARCHY_ICON, HIERARCHY_ICON);
            CTPlayerPrefs.SetBool(Constants.KEY_ENFORCE_32BIT_WINDOWS, ENFORCE_32BIT_WINDOWS);
            //CTPlayerPrefs.SetString(Constants.KEY_TTS_MACOS, TTS_MACOS);

            CTPlayerPrefs.Save();
        }

        #endregion
    }
}
// © 2017 crosstales LLC (https://www.crosstales.com)