﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class test : MonoBehaviour {

    TextToSpeech tts;
    void Start()
    {

        tts = GetComponent<TextToSpeech>();
	}
//	public void Speak(Text textToSpeak)
//	{
//		tts.Speak(textToSpeak.text, (string msg) =>
//			//		tts.Speak("Yeah it is working", (string msg) =>
//			{
//				tts.ShowToast(msg);
//			});
//	}


	public void Speak(string textToSpeak)
	{
//		Debug.LogError ("called " + Time.time );
		tts.Speak(textToSpeak, (string msg) =>
			//		tts.Speak("Yeah it is working", (string msg) =>
			{
				tts.ShowToast(msg);
			});
	}


    public void ChangeSpeed()
    {
        tts.SetSpeed(0.5f);
    }
    public void ChangeLanguage()
    {
        tts.SetLanguage(TextToSpeech.Locale.UK);
    }
    public void ChangePitch()
    {
        tts.SetPitch(0.6f);
    }
}
